import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import AppContainer from './src/navigation/AppContainer';

import { createStore } from 'redux';
import { Provider } from 'react-redux';

const initialState = {
  status: [],
}

const reducer = (state = initialState, action) => {
  if (action.type == 'setStatus') {
    return Object.assign({}, state, { status: [] });
  }
  return state;
}

const store = createStore(reducer);

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <SafeAreaView style={{ flex: 1 }}>
          <AppContainer />
        </SafeAreaView>
      </ Provider>
    );
  }
}