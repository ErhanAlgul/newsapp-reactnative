import React from 'react';
import { View } from 'react-native';

const ItemSection = (props) => {
    return (
        <View style={styles.viewStyle}>
            {props.children}
        </View>
    );
}

const styles = {
    viewStyle: {
        borderWidth: 0.3,
        borderColor: "gainsbro",
        shadowColor: "black",
        borderBottomWidth: 0.8,
        padding: 1

    }
}

export default ItemSection;