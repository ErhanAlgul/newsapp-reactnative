import { Dimensions, StyleSheet } from "react-native"

const { width, height } = Dimensions.get('window');

const List = StyleSheet.create({
    headerContainer: {
        flexDirection: "column",
        justifyContent: "space-between",
        paddingLeft: 3
    },
    headerText: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: "center",
        textTransform: 'uppercase'

    },
    imageStyle: {
        height: height * 0.30,
        flex: 1,
        width: null,
    },
    shareListStyle: {
        fontSize: 16,
        color: '#fff',
        position: 'absolute',
        top: 2,
        right: 0,
        padding: 5,
        fontWeight: 'bold'
    }
})

const Content = StyleSheet.create({
    headerContainer: {
        flexDirection: "column",
        justifyContent: "space-between",
    },
    headerText: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: "center",


    },
    imageStyle: {
        height: height * 0.30,
        width: null,
    }

})

const Search = StyleSheet.create({
    container: {
        width: width * 0.95,
        height: height * 0.06,
        backgroundColor: '#dcdcdc',
        borderRadius: 15,
        margin: 10,
    },
    searchInput: {
        width: '100%',
        height: '100%',
        paddingLeft: 12,
        fontSize: 16,
    }
})


export { List, Content, Search }