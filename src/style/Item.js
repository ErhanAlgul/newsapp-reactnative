import React from 'react';
import { View } from 'react-native';

const Item = (props) => {
    return (
        <View style={styles.viewStyle}>
            {props.children}
        </View>
    );
}

const styles = {
    viewStyle: {
        borderWidth: 0,
        borderColor: "white",
        borderBottomWidth: 0,
        margin: 10
    }
}

export default Item;