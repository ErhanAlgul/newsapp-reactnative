import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import NewsList from '../components/NewsList';
import NewsContent from '../components/NewsContent';

const Stack = createStackNavigator();

const App = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="NewsList">
                <Stack.Screen name="NewsList" component={NewsList} />
                <Stack.Screen name="NewsContent" component={NewsContent} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;
