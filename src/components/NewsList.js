import React from 'react';
import { Text, View, ScrollView, Share, TextInput, TouchableOpacity, TouchableWithoutFeedback, Image } from 'react-native';
import axios from 'axios';

import Item from '../style/Item';
import ItemSection from '../style/ItemSection';
import { List, Search } from '../style/Styles';

class NewsList extends React.Component {
    componentDidMount() {
        axios.get("http://newsapi.org/v2/everything?q=crypto&sortBy=popularity&apiKey=939fbb30b11d4e94919d07ff28a89453").then(response => {
            this.setState({ newsList: response.data.articles, newsFiltered: response.data.articles })
        });
    };

    constructor(props) {
        super(props);
        this.state = {
            newsList: [],
            newsFiltered: []
        };
    }

    // for news sharing
    sharearticle = async article => {
        try {
            await Share.share({
                message: article
            })
        } catch (error) {
            console.log(error)
        }
    }

    // to search a sepecific news
    searchNews(textToSearch) {
        this.setState({
            newsFiltered: this.state.newsList.filter(i =>
                i.title.toLowerCase().includes(textToSearch.toLowerCase()),
            ),
        });
    }

    renderList = () => {
        return this.state.newsFiltered.map((author) => {
            return (
                <Item>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('NewsContent', {
                        urlToImage: author.urlToImage,
                        title: author.title,
                        description: author.description,
                        content: author.content,
                    })}>
                        <ItemSection>
                            <Image key={author} style={List.imageStyle} source={{ uri: author.urlToImage }}></Image>
                            <Text style={List.shareListStyle} onPress={() => this.sharearticle(author.url)}>Share</Text>
                        </ItemSection>
                    </TouchableOpacity>
                    <ItemSection>
                        <View style={List.headerContainer}>
                            <TouchableWithoutFeedback>
                                <Text key={author} style={List.headerText} onPress={() => this.props.navigation.navigate('NewsContent', {
                                    urlToImage: author.urlToImage,
                                    title: author.title,
                                    description: author.description,
                                    content: author.content,
                                })} >{author.title}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </ItemSection>
                </Item>
            );
        });
    };


    render() {
        return (
            <ScrollView>
                <View style={Search.container}>
                    <TextInput
                        style={Search.searchInput}
                        initialized
                        placeholder='Search Here...'
                        onChangeText={text => {
                            this.searchNews(text);
                        }} />
                </View>
                {this.renderList()}
            </ScrollView>
        )
    }
};


export default NewsList;