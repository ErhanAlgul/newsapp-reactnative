import React from 'react';
import { Text, Image } from 'react-native'

import Item from '../style/Item';
import ItemSection from '../style/ItemSection';
import { Content } from '../style/Styles';

const NewsContent = props => {
    const urlToImage = props.route.params.urlToImage;
    const title = props.route.params.title;
    const description = props.route.params.description;
    const content = props.route.params.content;
    return (
        <Item>
            <ItemSection>
                <Text style={Content.headerText}> {title} </Text>
            </ItemSection>
            <ItemSection>
                <Image style={Content.imageStyle} source={{ uri: urlToImage }} />
            </ItemSection>
            <ItemSection>
                <Text style={Content.headerText}> {description} </Text>
            </ItemSection>
            <ItemSection>
                <Text style={Content.headerText}> {content} </Text>
            </ItemSection>
        </Item>
    );
};


export default NewsContent;